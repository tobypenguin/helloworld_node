const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('HelloWorld')
})

app.get('/me', (req, res) => {
  res.send('HelloMe')
})

app.get('/json', (req, res) => {
  res.json({
    id: 1,
    name: 'Ipad'
  })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
